package com.fernapp.raup.windowmanagement.server;

import java.io.Serializable;

/**
 * A chunk of encoded video data. Must be decoded by a apropriate decoder. Depending on the decoder, the data of the
 * chunk might contain content that is larger then what is specified by widht and height if the encoder uses padding. It
 * might also be smaller if only the changed part of the window has been send.
 * 
 * @author Markus
 */
public class VideoStreamChunk implements Serializable {

	private int width;
	private int height;
	private int encoderType;
	/**
	 * Indicates that the encoder has been resetted (maybe due to a change of the window size). In the first stream
	 * chunk from an encoder this field is irrelevant (can be false or true).
	 */
	private boolean reset;
	private byte[] data;

	public VideoStreamChunk() {
		// default constructor
	}

	public VideoStreamChunk(int width, int height, int encoderType, boolean reset, byte[] data) {
		this.width = width;
		this.height = height;
		this.encoderType = encoderType;
		this.reset = reset;
		this.data = data;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getEncoderType() {
		return encoderType;
	}

	public void setEncoderType(int encoderType) {
		this.encoderType = encoderType;
	}

	public boolean isReset() {
		return reset;
	}

	public void setReset(boolean reset) {
		this.reset = reset;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

}

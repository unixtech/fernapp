package com.fernapp.raup.windowmanagement.client;

import java.io.Serializable;

import com.fernapp.raup.windowmanagement.server.VideoStreamChunk;

/**
 * Requests that the window be resized (can be ignored by the server). Clients should only fire it for user triggered
 * resizes and not for a resize that was caused internally (i.e. by a differently sized {@link VideoStreamChunk}).
 * 
 * @author Markus
 */
public class WindowResizeRequest implements Serializable {

	private String windowId;
	private int width;
	private int height;

	public WindowResizeRequest() {
		// default constructor
	}

	public WindowResizeRequest(String windowId, int width, int height) {
		this.windowId = windowId;
		this.width = width;
		this.height = height;
	}

	@Override
	public String toString() {
		return "[windowId=" + windowId + ", width=" + width + ", height=" + height + "]";
	}

	public String getWindowId() {
		return windowId;
	}

	public void setWindowId(String windowId) {
		this.windowId = windowId;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

}

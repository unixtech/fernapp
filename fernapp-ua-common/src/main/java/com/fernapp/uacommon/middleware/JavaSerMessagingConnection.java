package com.fernapp.uacommon.middleware;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.uacommon.middleware.channel.DataChannel;
import com.fernapp.uacommon.middleware.channel.DataChannel.WriteJob;

/**
 * A {@link MessagingConnection} implementation based on standard java serialization.
 * 
 * @author Markus
 */
public class JavaSerMessagingConnection extends AbstractMessagingConnection {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private final DataChannel dataChannel;
	private final DataInputStream dataInputStream;
	private final DataOutputStream dataOutputStream;

	public JavaSerMessagingConnection(DataChannel dataChannel, String name) {
		super(name);
		this.dataChannel = dataChannel;
		dataInputStream = new DataInputStream(new BufferedInputStream(dataChannel.getInputStream()));
		dataOutputStream = new DataOutputStream(new BufferedOutputStream(dataChannel.getOutputStream()));
	}

	/**
	 * @see com.fernapp.uacommon.middleware.AbstractMessagingConnection#getDataChannel()
	 */
	@Override
	protected DataChannel getDataChannel() {
		return dataChannel;
	}

	/**
	 * @see com.fernapp.uacommon.middleware.AbstractMessagingConnection#writeMessage(java.lang.Object)
	 */
	@Override
	protected void writeMessage(Object message) throws IOException {
		// TODO evaluate performance
		// This allows several messages to be serialized in parallel, while waiting for
		// the connection to become available for writes. On the other hand, not writing
		// directly on the output stream (using the byte array) is slightly slower for
		// a single message.

		final byte[] data = serializeMessage(message);
		if (log.isTraceEnabled()) {
			log.trace("writeMessage: Serialized message size is " + data.length + " bytes");
		}

		try {
			dataChannel.write(new WriteJob() {
				public void write() throws IOException {
					dataOutputStream.writeInt(data.length);
					dataOutputStream.write(data);
					dataOutputStream.flush();
				}
			}, 5, TimeUnit.SECONDS);
		} catch (TimeoutException e) {
			log.warn("Write has timed out. Other side is probably gone.");
			close();
		}
	}

	/**
	 * @see com.fernapp.uacommon.middleware.AbstractMessagingConnection#readMessage()
	 */
	@Override
	protected Object readMessage() {
		Object obj = null;
		try {
			int length = dataInputStream.readInt();
			byte[] data = new byte[length];
			dataInputStream.readFully(data, 0, length);
			obj = deserializeMessage(data);
		} catch (EOFException e) {
			log.info("The other side has closed the connection");
			close();
		} catch (IOException e) {
			if (isOpen()) {
				log.error("Receive failed while connection was open", e);
				close();
			} else {
				// expected exception. will be thrown when connection was closed by us
				// asynchronously
			}
		}

		return obj;
	}

	private byte[] serializeMessage(Object message) throws IOException {
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		ObjectOutputStream oout = new ObjectOutputStream(bout);

		oout.flush();
		oout.writeObject(message);
		oout.flush();
		// disable the "Caching Objects in the Stream" feature
		oout.reset();

		byte[] data = bout.toByteArray();
		oout.close();
		return data;
	}

	private Object deserializeMessage(byte[] data) throws IOException {
		try {
			ObjectInputStream oin = new ObjectInputStream(new ByteArrayInputStream(data));
			Object obj = oin.readObject();
			oin.close();
			return obj;
		} catch (ClassNotFoundException e) {
			log.error("Receive failed", e);
			return null;
		}
	}

}

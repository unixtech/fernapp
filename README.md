# Accessing Linux desktop applications in a web browser

fernapp makes it possible to run a GUI application on a Linux server and access it from anywhere via web browser!

The official project website is at [http://www.fernapp.com](http://www.fernapp.com).
The documentation is located [here](https://fernapp.atlassian.net/wiki/display/FERN).

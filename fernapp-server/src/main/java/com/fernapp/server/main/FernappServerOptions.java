package com.fernapp.server.main;

public class FernappServerOptions {

	/**
	 * the port used by the internal HTTP web server
	 */
	private int httpPort = 8080;

	/**
	 * the RAUP port
	 */
	private int tcpRaupPort = 4711;

	/**
	 * A passphrase for client authentication.
	 */
	private String passphrase;

	/**
	 * The path to the fernapp server runtime directory. Usually this is the working directory, but might be different
	 * when executing unit tests.
	 */
	private String runtimeDir = ".";

	private boolean analyticsEnabled = true;

	public int getHttpPort() {
		return httpPort;
	}

	public void setHttpPort(int httpPort) {
		this.httpPort = httpPort;
	}

	public int getTcpRaupPort() {
		return tcpRaupPort;
	}

	public void setTcpRaupPort(int port) {
		this.tcpRaupPort = port;
	}

	public String getPassphrase() {
		return passphrase;
	}

	public void setPassphrase(String passphrase) {
		this.passphrase = passphrase;
	}

	public String getRuntimeDir() {
		return runtimeDir;
	}

	public void setRuntimeDir(String runtimeDir) {
		this.runtimeDir = runtimeDir;
	}

	public boolean isAnalyticsEnabled() {
		return analyticsEnabled;
	}

	public void setAnalyticsEnabled(boolean analyticsEnabled) {
		this.analyticsEnabled = analyticsEnabled;
	}

}

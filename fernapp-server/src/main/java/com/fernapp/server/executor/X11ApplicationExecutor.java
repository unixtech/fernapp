package com.fernapp.server.executor;

import java.util.Collection;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fernapp.raup.windowmanagement.client.InputEvent;
import com.fernapp.raup.windowmanagement.client.KeyboardInputEvent;
import com.fernapp.raup.windowmanagement.client.PointerButtonEvent;
import com.fernapp.raup.windowmanagement.client.WindowResizeRequest;
import com.fernapp.raup.windowmanagement.server.WindowSettings;
import com.fernapp.server.encoding.WindowContent;
import com.fernapp.server.executor.X11.KeycodeConverter;
import com.fernapp.server.executor.X11.ProcessManager;
import com.fernapp.uacommon.measurement.DelayCategory;
import com.fernapp.uacommon.measurement.MeasurementReceiver;
import com.google.common.base.Preconditions;

/**
 * @author Markus
 * 
 */
public class X11ApplicationExecutor implements ApplicationExecutor {

	private static final Logger log = LoggerFactory.getLogger(X11ApplicationExecutor.class);
	private static final String PROCESS_LOG_PREFIX = "com.fernapp.server.executor.";
	private static final Logger nativeLog = LoggerFactory.getLogger(PROCESS_LOG_PREFIX + "Native");

	// os.arch is not necessarily the JVM arch but we enforce a 64-bit JVM for 64-bit Linux in fernapp-server.sh
	private static final boolean JVM_64_BIT = System.getProperty("os.arch").equals("amd64");

	static {
		try {
			if (JVM_64_BIT) {
				log.info("Running in a 64-bit environment");
			} else {
				log.info("Running in a 32-bit environment");
			}
			System.loadLibrary("fernwm");
		} catch (UnsatisfiedLinkError e) {
			log.error(e.toString() + " (java.library.path: " + System.getProperty("java.library.path") + ")");
			throw e;
		}
	}

	/**
	 * Start a classic X server instead of Xvfb.
	 */
	private final boolean useClassicX = false;

	private final String displayPort;
	private final String runtimeDir;
	private volatile WindowEventListener windowEventListener;
	private volatile MeasurementReceiver measurementReceiver;
	private volatile boolean active = false;
	private final CountDownLatch shutdownLatch = new CountDownLatch(1);

	public X11ApplicationExecutor(String runtimeDir) {
		this.runtimeDir = runtimeDir;
		this.displayPort = System.getenv("DISPLAY");
	}

	@Override
	public void init(WindowEventListener wel, MeasurementReceiver mr) {
		Preconditions.checkState(!active);

		log.info("Executor initializing");
		this.windowEventListener = wel;
		this.measurementReceiver = mr;

		// start xserver
		try {
			if (useClassicX) {
				log.info("Starting X server");
				ProcessManager.INSTANCE.startProcess(LoggerFactory.getLogger(PROCESS_LOG_PREFIX + "X"), "/usr/bin/X",
						displayPort);
			} else {
				log.info("Starting Xvfb server");
				ProcessManager.INSTANCE.startProcess(LoggerFactory.getLogger(PROCESS_LOG_PREFIX + "Xvfb"),
						"/usr/bin/Xvfb", displayPort, "-screen", "0", "1600x900x24");
			}
			TimeUnit.SECONDS.sleep(2);
		} catch (Exception e) {
			throw new RuntimeException("Failed to start X server", e);
		}

		// init fernwm before starting the application - we need to collect all events!
		boolean initSuccessful = _init(displayPort);
		if (!initSuccessful) {
			throw new RuntimeException("libfernwm x11connector failed to initialize");
		}

		log.info("Starting event loop thread");
		new Thread(new Runnable() {
			public void run() {
				try {
					_eventProcessingLoop();
				} catch (Exception e) {
					log.error("Event loop died", e);
				}

				try {
					// when event loop has finished we unblock the call to 'shutdown'
					shutdownLatch.countDown();
				} catch (Exception e) {
					log.error("Application executor clean up had an error", e);
				}
			}
		}, "x11EventLoop").start();

		log.info("Starting application");
		new Thread(new Runnable() {
			public void run() {
				try {
					log.info("Starting xsettingsd");
					String xsettingsd = JVM_64_BIT ? "xsettingsd64" : "xsettingsd32";
					ProcessManager.INSTANCE.startProcess(LoggerFactory.getLogger(PROCESS_LOG_PREFIX + "xsettingsd"),
							runtimeDir + "/" + xsettingsd, "-c", runtimeDir + "/xsettingsd.conf");

					log.info("Setting keymap");
					ProcessManager.INSTANCE.startProcess(LoggerFactory.getLogger(PROCESS_LOG_PREFIX + "setxkbmap"),
							"setxkbmap", "de");

				} catch (Exception e) {
					log.error("Application startup failed", e);
				}
			}
		}, "applicationStart").start();

		active = true;
		log.info("Executor init finished");
	}

	@Override
	public void shutdown() {
		Preconditions.checkState(active);
		log.info("Application executor is shutting down");
		active = false;

		// disables the event loop and clean up of native stuff
		_shutdown();

		// wait until event loop has finished
		try {
			shutdownLatch.await();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		log.info("Executor has been shut down");
	}

	private native boolean _init(String displayPort);

	private native void _eventProcessingLoop();

	private native void _shutdown();

	/**
	 * To be called from native code for logging purpose.
	 */
	public static void _logDebug(String message, int logLevel) {
		switch (logLevel) {
		case 0:
			nativeLog.trace(message);
			break;
		case 1:
			nativeLog.debug(message);
			break;
		case 2:
			nativeLog.info(message);
			break;
		case 3:
			nativeLog.warn(message);
			break;
		case 4:
			nativeLog.error(message);
			break;
		default:
			nativeLog.error("Invalid log level " + logLevel + ". " + message);
			break;
		}
	}

	public WindowContent captureWindowContent(String windowId) {
		Preconditions.checkState(active);
		return _captureWindowContent(windowId);
	}

	private native WindowContent _captureWindowContent(String windowId);

	public void sendInputEvent(InputEvent e) {
		Preconditions.checkState(active);
		if (e.getPointerPositionLeft() != null && e.getPointerPositionTop() != null) {
			movePointer(e.getWindowId(), e.getPointerPositionLeft(), e.getPointerPositionTop());
		}

		if (e instanceof PointerButtonEvent) {
			PointerButtonEvent pe = (PointerButtonEvent) e;
			pointerButtonAction(e.getWindowId(), e.getPointerPositionLeft(), e.getPointerPositionTop(),
					pe.isButtonPressed(), pe.getButtonNumber());
		}
		if (e instanceof KeyboardInputEvent) {
			KeyboardInputEvent ke = (KeyboardInputEvent) e;
			keyboardAction(ke.getWindowId(),
					KeycodeConverter.getKeysym(ke.getKeyCode(), ke.getKeyChar(), ke.getKeyLocation()), ke.isPressed());
		}
	}

	private native void movePointer(String windowId, int x, int y);

	private native void pointerButtonAction(String windowId, int x, int y, boolean buttonPressed, int buttonNumber);

	private native void keyboardAction(String windowId, long keysym, boolean pressed);

	public void requestWindowResize(WindowResizeRequest wrr) {
		Preconditions.checkState(active);
		requestWindowResize(wrr.getWindowId(), wrr.getWidth(), wrr.getHeight());
	}

	private native void requestWindowResize(String windowId, int width, int height);

	@Override
	public void requestWindowClose(String windowId) {
		Preconditions.checkState(active);
		_requestWindowClose(windowId);
	}

	private native void _requestWindowClose(String windowId);

	public WindowEventListener getWindowEventListener() {
		return windowEventListener;
	}

	public void receiveNativeMeasurement(int category, long delay, long dataSize) {
		Preconditions.checkState(active);
		DelayCategory delayCategory;
		if (category == 0) {
			delayCategory = DelayCategory.WAIT;
		} else if (category == 1) {
			delayCategory = DelayCategory.CAPTURE;
		} else if (category == 2) {
			delayCategory = DelayCategory.ENCODE;
		} else if (category == 3) {
			delayCategory = DelayCategory.APPLICATION;

		} else {
			throw new IllegalArgumentException();
		}
		measurementReceiver.receiveMeasurement(delayCategory, delay, dataSize);
	}

	public native Collection<WindowSettings> getOpenWindows();

	public native void lockWindowData();

	public native void unlockWindowData();

}

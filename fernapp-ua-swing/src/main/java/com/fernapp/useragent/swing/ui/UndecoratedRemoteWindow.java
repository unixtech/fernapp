package com.fernapp.useragent.swing.ui;

import javax.swing.JWindow;

/**
 * A container container that is not decorated and does not appear in the task bar.
 * 
 * @author Markus
 */
public class UndecoratedRemoteWindow extends AbstractAwtWindowWrapper {

	public UndecoratedRemoteWindow() {
		super(new JWindow());
	}

	/**
	 * @see com.fernapp.useragent.ui.WindowContainer#setTitle(java.lang.String)
	 */
	public void setTitle(String title) {
		// ignore
	}

}

#include <stdlib.h>
#include <signal.h>
#include "../util/windowDataManager.h"
#include "jniConnector.h"


void onWindowSettingsChange(WindowData *windowData, Bool isNewWindow) {
	JNIEnv *env = getJniEnv();
	if(env != NULL) {
		jobject windowSettings = buildWindowSettingsObject(env, windowData);
		(*env)->CallVoidMethod(env, object_WindowEventListener, method_WindowEventListener_onWindowSettingsChange, windowSettings, isNewWindow);
		(*env)->DeleteLocalRef(env, windowSettings);

		handlePotentialJniErrors(env);
	}
}


void onDestroy(WindowId windowId) {
	JNIEnv *env = getJniEnv();
	if(env != NULL) {
		jstring windowIdString = buildWindowIdString(env, windowId);
		(*env)->CallVoidMethod(env, object_WindowEventListener, method_WindowEventListener_onWindowDestroyed, windowIdString);
		(*env)->DeleteLocalRef(env, windowIdString);

		handlePotentialJniErrors(env);
	}
}


void onWindowContentUpdate(WindowId windowId, int x, int y, int width, int height) {
	JNIEnv *env = getJniEnv();
	if(env != NULL) {
		jstring windowIdString = buildWindowIdString(env, windowId);
		jobject damageReport = (*env)->NewObject(env, class_DamageReport, method_DamageReport_ctor, x, y, width, height);
		(*env)->CallVoidMethod(env, object_WindowEventListener, method_WindowEventListener_onWindowContentUpdate, windowIdString, damageReport);
		(*env)->DeleteLocalRef(env, damageReport);
		(*env)->DeleteLocalRef(env, windowIdString);

		handlePotentialJniErrors(env);
	}
}


void reportMeasurement(long category, long long delay, long long dataSize) {
	JNIEnv *env = getJniEnv();
	if(env != NULL) {
		(*env)->CallVoidMethod(env, object_X11ApplicationExecutor, method_receiveNativeMeasurement, (jint) category, (jlong) delay, (jlong) dataSize);
	} else {
		logWarn("Measurement ignored (no JNI)");
	}
}

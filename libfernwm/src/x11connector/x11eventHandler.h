#ifndef X11EVENTHANDLER_H_
#define X11EVENTHANDLER_H_

/**
 * Handles the events from X11.
 */

#include <util/common.h>


void x11EventLoop();


#endif

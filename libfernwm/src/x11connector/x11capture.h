#ifndef X11CAPTURE_H_
#define X11CAPTURE_H_

#include <X11/Xlib.h>
#include "../util/common.h"


/**
 * Captures the current window content. Everyone holding a reference to it must increment referencesHeld.
 * freeWindowContent must be called when it is no longer needed.
 * Returns NULL if no such window exists.
 */
WindowContent *x11CaptureWindowContent(WindowId windowId);

void setupCapturing(Display *dpy, WindowId windowId);


#endif /* X11CAPTURE_H_ */
